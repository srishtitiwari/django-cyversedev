from . import Sharing
from . import Path
from .CustomEncoder import CustomEncoder
from . import ShareRequest
from . import UnshareRequest
from . import CoverageGraphAnalysisRequest
from . import FilePermissionRequest
from .EncoderPaths import EncoderPaths


class RequestCreator:
    @staticmethod
    def create_share_folder_request(filepath, user, permission):
        share_data = Sharing()
        share_data.user = user
        p = Path()
        p.path = filepath
        p.permission = permission
        share_data.paths.append(p)
        share_req = ShareRequest()
        share_req.sharing.append(share_data)
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(share_req)

    @staticmethod
    def create_unshare_request(filepath, user):
        unshare_data = Sharing()
        unshare_data.user = user
        p = Path()
        p.path = filepath
        unshare_data.paths.append(p)
        unshare_req = UnshareRequest()
        unshare_req.unshare.append(unshare_data)
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(unshare_req)

    @staticmethod
    def create_coveragegraph_request(name, appid, systemid, debug, createOutputDir, archiveLogs, outputdirpath,
                                  notify, inputfilepath, outputfilename,config_input_id,config_output_id):
        requestdata = CoverageGraphAnalysisRequest()
        requestdata.name = name
        requestdata.app_id = appid
        requestdata.system_id = systemid
        requestdata.debug = debug
        requestdata.create_output_subdir = createOutputDir
        requestdata.archive_logs = archiveLogs
        requestdata.output_dir = outputdirpath
        requestdata.notify = notify
        config = dict()
        config[config_input_id] = inputfilepath
        config[config_output_id] = outputfilename
        requestdata.config = config
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(requestdata)


    @staticmethod
    def create_config(config_data):
        config_request = []
        if config_data is not None:
            for data in config_data:
                config_request[data] = config_data[data]
        return config_data

    @staticmethod
    def create_file_permission_request(filepaths):
        filepermisison_request = FilePermissionRequest()
        path_data = []
        paths = filepaths.split(',')
        for path in paths:
            if path is not None:
                path_data.append(path)
        filepermisison_request.paths = paths
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(filepermisison_request)

    @staticmethod
    def getFileId(filepath):
        paths_list = EncoderPaths()
        paths_list.paths.append(filepath)
        custom_encoder = CustomEncoder()
        return custom_encoder.encode(paths_list)

