import math
from datetime import datetime
from . import FileData
from . import FileSystemInfo
from . import ShareResponse
from . import Sharing
from . import SharingPath
from . import Unshare
from . import UnshareResponse
from . import RetrieveAnalysesResponse
from . import Analyses
from . import CoverageGraphAnalysisResponse
from . import FilePermissionPath
from . import FilePermissionResponse
from . import UserPermission
from . import FileIDInfo, MetaData, MetaDataView, UserDetails, basepaths


class ResponseParser:
    # Parses the response received from the apis
    # Converts them into more structured format

    @staticmethod
    def parse_response(response):

        file_data = FileData.FileData()
        file_data.hasSubDirs = response['hasSubDirs']
        file_data.total = response['total']
        file_data.totalBad = response['totalBad']
        file_data.folders = ResponseParser.parse_folders(response['folders'])
        file_data.files = ResponseParser.parse_files(response['files'])
        file_data.fileSystemInfo = ResponseParser.parse_file_system_info(response)
        return file_data

    @staticmethod
    def parse_userDetailresponse(response):
        details = UserDetails.UserDetails()

        for root in response['roots']:
            fileinfo = FileIDInfo.FileIDInfo()
            fileinfo.id = root['id']
            fileinfo.path = root['path']
            fileinfo.label = root['label']
            fileinfo.datecreated = root['date-created']
            fileinfo.datemodified = root['date-modified']
            fileinfo.permission = root['permission']
            fileinfo.hasSubDirs = root['hasSubDirs']
            details.roots.append(fileinfo)

        basepaths_json = response['base-paths']

        basepath_obj = basepaths.basepaths()
        basepath_obj.user_trash_path = basepaths_json['user_trash_path']
        basepath_obj.user_home_path = basepaths_json['user_home_path']
        basepath_obj.base_trash_path =  basepaths_json['base_trash_path']
        details.base_paths = basepath_obj

        return details

    @staticmethod
    def parse_fileIDresponse(response):
        fileID_data = FileIDInfo.FileIDInfo()

        path = ''
        for path_file in response['paths']:
            path = path_file

        if 'paths' in response:
            response_path = response['paths'][path]
        if 'infoType' in response_path:
            fileID_data.infoType = response_path['infoType']
        if 'path' in response_path:
            fileID_data.path = response_path['path']
        if 'share-count' in response_path:
            fileID_data.sharecount = response_path['share-count']
        if 'date-created' in response_path:
            fileID_data.datecreated = ResponseParser.convert_date(response_path['date-created'])
        if 'md5' in response_path:
            fileID_data.md5 = response_path['md5']
        if 'permission' in response_path:
            fileID_data.permission = response_path['permission']
        if 'date-modified' in response_path:
            fileID_data.datemodified = ResponseParser.convert_date(response_path['date-modified'])
        if 'type' in response_path:
            fileID_data.type = response_path['type']
        if 'file-size' in response_path:
            fileID_data.filesize = response_path['file-size']
        if 'label' in response_path:
            fileID_data.label = response_path['label']
        if 'id' in response_path:
            fileID_data.id = response_path['id']
        if 'content-type' in response_path:
            fileID_data.contenttype = response_path['content-type']
        if 'ids' in response_path:
            fileID_data.ids = response['ids']
        if 'dir-count' in response_path:
            fileID_data.dircount = response_path['dir-count']
        if 'file-count' in response_path:
            fileID_data.filecount = response_path['file-count']
        return fileID_data

    @staticmethod
    def parse_MetaDataresponse(response):
        avusmodel = MetaDataView.avus()
        metadata_list = []
        if 'avus' in response:
            for avus in response['avus']:
                if 'unit' in avus:
                    if avus['unit'] == 'integratedGenomeBrowser':
                        metaData = MetaData.MetaData()
                        if 'attr' in avus:
                            metaData.attr = avus['attr']
                        if 'value' in avus:
                            metaData.value = avus['value']
                        if 'id' in avus:
                            metaData.id = avus['id']
                        if 'target_id' in avus:
                            metaData.target_id = avus['target_id']
                        if 'modified_by' in avus:
                            metaData.modified_by = avus['modified_by']

                        metaData.unit = avus['unit']
                        metadata_list.append(metaData)
            avusmodel.avus = metadata_list
            return avusmodel

    @staticmethod
    def parse_file_system_info(response):
        file_info = FileSystemInfo.FileSystemInfo()
        file_info.infoType = response['infoType']
        file_info.path = response['path']
        file_info.dateCreated = ResponseParser.convert_date(response['date-created'])
        file_info.permission = response['permission']
        file_info.dateModified = ResponseParser.convert_date(response['date-modified'])
        file_info.fileSize = ResponseParser.size_conversion(response['file-size'])
        file_info.badName = response['badName']
        file_info.isFavorite = response['isFavorite']
        file_info.label = response['label']
        file_info.id = response['id']
        return file_info
    
    @staticmethod
    def parse_files(files_data):
        files = []
        for f in files_data:
            if f is not None:
                file = ResponseParser.parse_file_system_info(f)
            files.append(file)
        return files

    @staticmethod
    def parse_folders(folders_data):
        folders = []
        for f in folders_data:
            if f is not None:
                folder = ResponseParser.parse_file_system_info(f)
            folders.append(folder)
        return folders

    @staticmethod
    def size_conversion(file_size):
        size_unit = ['Bytes', 'KB', 'MB', 'GB', 'TB']
        if file_size == 0:
            return '0 Byte'
        i = int(math.floor(math.log(file_size) / math.log(1024)))
        size = round((file_size / math.pow(1024, i)), 2)
        return str(size) + " " + size_unit[i]
    
    @staticmethod
    def convert_date(time):
        if time > 0:
            datetimeInfo = datetime.fromtimestamp(time/1000.0).strftime("%m-%d-%Y %H:%M:%S")
            return datetimeInfo

    @staticmethod
    def parse_share_response(response):
        sharing_res = []
        for res in response:
            if res is not None:
                sharing = Sharing()
                sharing.user = res['user']
                sharing.paths = ResponseParser.parse_sharing_path_response(res['sharing'])
                sharing_res.append(sharing)
        return sharing_res



    @staticmethod
    def parse_sharing_path_response(path_response):
        sharing_paths = []
        if path_response is not None:
            for response in path_response:
                if response is not None:
                    p = SharingPath()
                    p.path = response['path']
                    p.permission = response['permission']
                    p.success = response['success']
                    sharing_paths.append(p)
        return sharing_paths

    @staticmethod
    def parse_unshare_response(response):
        unsharing_res = []
        for res in response:
            if res is not None:
                sharing = Sharing()
                sharing.user = res['user']
                sharing.paths = ResponseParser.parse_unsharing_path_response(res['unshare'])
                unsharing_res.append(sharing)
        return unsharing_res

    @staticmethod
    def parse_unsharing_path_response(path_response):
        unsharing_paths = []
        if path_response is not None:
            for response in path_response:
                if response is not None:
                    u = Unshare()
                    u.path = response['path']
                    u.success = response['success']
                    unsharing_paths.append(u)
        return unsharing_paths

    @staticmethod
    def parse_analyses_data(analyses_response):
        analyses_data = []
        if analyses_response is not None:
            for response in analyses_response:
                if response is not None:
                    analyses = Analyses()
                    analyses.description = response['description']
                    analyses.name = response['name']
                    analyses.can_share = response['can_share']
                    analyses.username = response['username']
                    analyses.app_id = response['app_id']
                    analyses.system_id = response['system_id']
                    analyses.app_disabled = response['app_disabled']
                    analyses.batch = response['batch']
                    analyses.enddate = ResponseParser.convert_date(int(response['enddate']))
                    analyses.status = response['status']
                    analyses.id = response['id']
                    analyses.startdate = ResponseParser.convert_date(int(response['startdate']))
                    analyses.app_description = response['app_description']
                    analyses.notify = response['notify']
                    analyses.resultfolderid = response['resultfolderid']
                    analyses.app_name = response['app_name']
                analyses_data.append(analyses)
        return analyses_data

    @staticmethod
    def parse_filepermission_paths(path_response):
        file_permission_paths = []
        if path_response is not None:
            for path_data in path_response:
                if path_data is not None:
                    permission_path = FilePermissionPath()
                    permission_path.path = path_data['path']
                    permission_path.user_permissions = ResponseParser.parse_permissions(path_data['user-permissions'])
                    file_permission_paths.append(permission_path)
        return file_permission_paths

    @staticmethod
    def parse_permissions(permission_response):
        user_permissions = []
        if permission_response is not None:
            for permission in permission_response:
                if permission is not None:
                    user_permission = UserPermission()
                    user_permission.user = permission['user']
                    user_permission.permission = permission['permission']
                    user_permissions.append(user_permission)
        return user_permissions

    @staticmethod
    def get_files(api_response):
        # Parses get file list api
        response = ResponseParser.parse_response(api_response)
        return response

    @staticmethod
    def parse_getUserDetails(api_response):
        # Parses get user detail api
        response = ResponseParser.parse_userDetailresponse(api_response)
        return response

    @staticmethod
    def parse_getFileID(api_response):
        # Parses get fileID api
        response = ResponseParser.parse_fileIDresponse(api_response)
        return response

    @staticmethod
    def parse_getMetaData(api_response):
        # Parses get fileID api
        response = ResponseParser.parse_MetaDataresponse(api_response)
        return response



    @staticmethod
    def share_response(response):
        # Parses share response
        share_data = ShareResponse()
        if response is not None:
            for res in response.values():
                if res is not None:
                    share_data.sharing = ResponseParser.parse_share_response(res)
        return share_data

    @staticmethod
    def unshare_response(response):
        #Parses unshare file response
        unshare_data = UnshareResponse()
        if response is not None:
            for res in response.values():
                unshare_data.sharing = ResponseParser.parse_unshare_response(res)
        return unshare_data

    @staticmethod
    def retrieve_analyses_history(response):
        # Parse the retrieved Analyses History
        analyses_data = RetrieveAnalysesResponse()
        if response is not None:
            analyses_data.analyses = ResponseParser.parse_analyses_data(response['analyses'])
            analyses_data.timestamp = ResponseParser.convert_date(int(response['timestamp']))
            analyses_data.total = response['total']
        return analyses_data

    @staticmethod
    def parse_coveragegraph_analysis_response(response):
        # Parse cover graph analysis reponse
        coveragegraph_response = CoverageGraphAnalysisResponse()
        coveragegraph_response.id = response['id']
        coveragegraph_response.name = response['name']
        coveragegraph_response.status = ['status']
        coveragegraph_response.start_date = ResponseParser.convert_date(response['start-date'])
        return coveragegraph_response

    @staticmethod
    def parse_file_permission_response(response):
        # parse file permission response
        permission_response = FilePermissionResponse()
        permission_response.paths = ResponseParser.parse_filepermission_paths(response['paths'])
        return response
