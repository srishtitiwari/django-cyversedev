#!/usr/bin/env python
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def getUserData(path, limit, offset, sortcol, sortdir):
    r = requests.get(
        settings.baseurl + '/secured/filesystem/paged-directory?path=/iplant/home/'+ settings.username + path + '&limit=' + limit + '&offset=' + offset + '&sort-col=' + sortcol + '&sort-dir=' + sortdir,
        headers={'Authorization': 'Bearer ' + settings.accesstoken})
    json_data = r.json()
    response = ResponseParser.get_files(json_data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res

