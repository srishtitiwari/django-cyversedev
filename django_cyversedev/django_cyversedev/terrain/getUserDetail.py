# get metadata
import requests
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder
from .. import settings

##
{'roots':
     [
         {'id': 'a8199ebc-d893-11e8-8691-000e1e0af2dc', 'path': '/iplant/home/srishtitiwari', 'label': 'srishtitiwari', 'date-created': 1540498932000, 'date-modified': 1540498932000, 'permission': 'own', 'hasSubDirs': True},
         {'id': '7fa53922-e104-11e3-80a7-6abdce5a08d5', 'path': '/iplant/home/shared', 'label': 'Community Data', 'date-created': 1310756895000, 'date-modified': 1363276169000, 'permission': 'read', 'hasSubDirs': True},
         {'id': '86d5a8d2-e102-11e3-bfb4-6abdce5a08d5', 'path': '/iplant/home', 'label': 'Shared With Me', 'date-created': 1265955755000, 'date-modified': 1472172640000, 'permission': 'read', 'hasSubDirs': True},
         {'id': 'ceb0f96e-8bbf-11e9-be80-000e1e0af2dc', 'path': '/iplant/trash/home/de-irods/srishtitiwari', 'label': 'Trash', 'date-created': 1560199153000, 'date-modified': 1560199153000, 'permission': 'own', 'hasSubDirs': True}
      ],
 'base-paths':
     {
         'user_home_path': '/iplant/home/srishtitiwari',
         'user_trash_path': '/iplant/trash/home/de-irods/srishtitiwari',
         'base_trash_path': '/iplant/trash/home/de-irods'
     }
 }

##

def getUserDetails(accessToken):
    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/root'
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + accessToken})
    json_Data = r.json()
    response = ResponseParser.parse_getUserDetails(json_Data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res

#getUserDetails('AT-3633-tqV5Nw3lpXJJc6djho3XpLsrU-hPdzsr')
