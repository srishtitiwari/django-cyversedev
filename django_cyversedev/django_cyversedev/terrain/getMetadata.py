# get metadata
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


##
{
    'irods-avus': [],
    'path': '/iplant/home/srishtitiwari/BigBed',
    'avus': []
}


##


def getMetadata(file_id):

    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/' + file_id + '/metadata'
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken})
    json_data = r.json()
    response = ResponseParser.parse_getMetaData(json_data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)

    # replace string irods_avus to irods-avus
    return res.replace("irods_avus","irods-avus")

