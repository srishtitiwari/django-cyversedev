# Print Your code here
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder
##


def getSharedData(path, limit, offset, sortcol, sortdir):

    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/paged-directory?path='+path+'&limit='+limit+'&offset='+offset+'&sort-col='+sortcol+'&sort-dir='+sortdir
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken})
    json_data = r.json()
    response = ResponseParser.get_files(json_data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res


#getSharedData('/iplant/home', '5', '0', 'NAME','ASC')