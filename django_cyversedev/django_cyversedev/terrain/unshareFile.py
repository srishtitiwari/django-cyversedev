# unshare file
import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def unshareFile(filepath, user):
    request_data = RequestCreator.create_unshare_request(filepath, user)
    req_url = 'https://de.cyverse.org/terrain/secured/share'
    r = requests.post(req_url, headers={'Authorization': 'BEARER ' +settings.accesstoken,
                                       'Content-type': 'application/json'},
                      data=request_data)
    response = ResponseParser.unshare_response(r.json())
    print(r.json())
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res


unshareFile('/iplant/home/srishtitiwari/sci_data', 'anonymous')
