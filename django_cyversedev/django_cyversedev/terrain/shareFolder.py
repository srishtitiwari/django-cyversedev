# share folder
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.CustomEncoder import CustomEncoder


def shareFolder(folderpath, user):
    permission = "read"
    request_data = RequestCreator.create_share_folder_request(folderpath, user, permission)
    req_url = 'https://de.cyverse.org/terrain/secured/share'
    r = requests.post(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken,
                                        'Content-type': 'application/json'},
                      data=request_data)
    share_response = ResponseParser.share_response(r.json())
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(share_response)
    return res


shareFolder('/iplant/home/srishtitiwari/sci_data', 'anonymous')
