# get community data
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def getCommunityData(filepath, limit, offset, sortcol, sortdir):
    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/paged-directory?path='+filepath+'&limit='+limit+'&offset='+offset+'&sort-col='+sortcol+'&sort-dir='+sortdir
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken})
    json_data = r.json()
    response = ResponseParser.get_files(json_data)
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res


#getCommunityData('/iplant/home/shared/', '5', '0', 'NAME', 'ASC')