# run coverage graph analysis
import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def runCoverageGraphAnalysis(name, appid, systemid, debug, createOutputDir, archiveLogs, outputdirpath, notify, inputfilepath, outputfilename):
    config_input_id = "91c4fecc-8c89-11e9-890e-008cfa5ae621_91c694bc-8c89-11e9-890e-008cfa5ae621"
    config_output_id = "91c4fecc-8c89-11e9-890e-008cfa5ae621_bd9d0bba-8d1b-11e9-8836-008cfa5ae621"
    request = RequestCreator.create_coveragegraph_request(name, appid, systemid, debug, createOutputDir, archiveLogs, outputdirpath, notify, inputfilepath, outputfilename,config_input_id,config_output_id)
    req_url = 'https://de.cyverse.org/terrain/analyses'
    r = requests.post(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken,
                                        'Content-Type': 'application/json'},
                      data=request)
    response = ResponseParser.parse_coveragegraph_analysis_response(r.json())
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res


runCoverageGraphAnalysis('IGB Bedtools Genome Coverage', '91c41516-8c89-11e9-890e-008cfa5ae621', 'de', 'false', 'false', 'false' , '/iplant/home/nowlanf/output', 'true', '/iplant/home/nowlanf/testFiles/babyBam_unsorted.bam', 'bedGraphAnalysis1.bedgraph' )