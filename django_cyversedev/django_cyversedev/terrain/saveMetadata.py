# save metadata
import requests
from ..terrain_model.ResponseParser import ResponseParser, MetaData, MetaDataView
from ..terrain_model.CustomEncoder import CustomEncoder
from ..terrain_model.MetDataAttributes import  MetDataAttributes
from .. import settings


def saveMetadata(fileid, json_data_attributes):

    # retrieve the existing metadata
    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/' + fileid + '/metadata'
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken})
    json_data = r.json()

    current_metadata = MetaDataView.avus()
    current_metadata = ResponseParser.parse_getMetaData(json_data)
    if current_metadata is None:
        current_metadata = MetaDataView.avus()
    # iterate the json and append the attributes to the current list
    for attribute in json_data_attributes:
        newmetadata = MetDataAttributes()
        newmetadata.attr = attribute["attr"]
        newmetadata.value = attribute["value"]
        newmetadata.unit = attribute["unit"]
        current_metadata.avus.append(newmetadata)

    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(current_metadata)

    # replace string irods_avus to irods-avus
    post_response = res.replace("irods_avus", "irods-avus")

    req_url = 'https://de.cyverse.org/terrain/secured/filesystem/' + fileid + '/metadata'
    r = requests.post(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken,
                                        'Content-Type': 'application/json'},
                      data=post_response)
    return post_response


