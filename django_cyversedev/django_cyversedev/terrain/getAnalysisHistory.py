# get analysis history
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def getAnalysisHistory(limit, offset, sortfield, sortdir):
    req_url = 'https://de.cyverse.org/terrain/analyses?limit='+limit+'&offset='+offset+'&sort-field='+sortfield+'&sort-dir='+sortdir
    r = requests.get(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken})
    response = ResponseParser.retrieve_analyses_history(r.json())
    custom_encoder = CustomEncoder()
    res = custom_encoder.encode(response)
    return res


getAnalysisHistory('10', '0', 'enddate', 'DESC')
