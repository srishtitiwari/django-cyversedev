# get metadata
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder
from ..terrain_model.RequestCreator import RequestCreator

##
{'paths': {'/iplant/home/srishtitiwari/BedGraph_HomoSapien.bedGraph':
               {'infoType': 'bed', 'path': '/iplant/home/srishtitiwari/BedGraph_HomoSapien.bedGraph',
                'share-count': 1, 'date-created': 1551802318000, 'md5': 'b2e0a82957531a85de5733870dba0a5e',
                'permission': 'own', 'date-modified': 1551802318000, 'type': 'file', 'file-size': 32558742,
                'label': 'BedGraph_HomoSapien.bedGraph', 'id': '67a35118-3f61-11e9-a36d-000e1e0af2dc',
                'content-type': 'text/plain'}}, 'ids': {}
 }
##


def getMetadata(filepath):
     req_url = 'https://de.cyverse.org/terrain/secured/filesystem/stat'
     r = requests.post(req_url, headers={'Authorization': 'BEARER ' + settings.accesstoken,
                                        'Content-type': 'application/json'}, data=RequestCreator.getFileId(filepath))
     json_data = r.json()
     response = ResponseParser.parse_getFileID(json_data)
     custom_encoder = CustomEncoder()
     res = custom_encoder.encode(response)
     return res