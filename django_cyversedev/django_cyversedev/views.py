from django.shortcuts import render
from django.http import HttpResponse
from .terrain import getUserData, getSharedData, getCommunityData
import json
from .terrain_model import CustomEncoder
from .terrain import getBasicAuth, getfileid, getMetadata, saveMetadata, getUserDetail
from . import settings
import sys


def index(request):
    context = {}
    return render(request, 'index.html', context)


def home(request):
    context = {}
    return render(request, 'django_cyversedev/middlepanel.html', context)


def loginCyverse(request):
    getBasicAuth.getBasicAuth()

    userData = getUserData.getUserData('', '20', '0', 'NAME', 'ASC')
    context = {
        "userData": userData
    }
    return render(request, 'base.html', context)


def base(request):
    response = None
    if request.is_ajax():
        try:
            # sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            response = getUserData.getUserData(path, limit, offset, sortcol, sortdir)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def oauth(request):
    context = {}
    return render(request, 'oauth.html', context)

def getFileIdApi(request):
    response = None

    if request.is_ajax():
        try:
            path = request.GET.get('url')
            if path is None:
                raise Exception("The request header does not contain Path")

            response = getfileid.getMetadata(path)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        #raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)


def retrieveMetaData(request):
    response = None

    if request.is_ajax():
        try:
            fileId = request.GET.get('fileId')
            if fileId is None:
                raise Exception("The request header does not contain fileId")

            response = getMetadata.getMetadata(fileId)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        #raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)

def saveMetaData(request):
    # fileId = ""
    # data = [{"attr":"Genome","unit":"integratedGenomeBrowser","value":"somegenome"},{"attr":"foreground","unit":"integratedGenomeBrowser","value":"1211212"},{"attr":"background","unit":"integratedGenomeBrowser","value":"1211212"}]

    response = None
    if request.is_ajax():
        try:
            fileId = request.POST['fileId']
            data_attrib = json.loads(request.POST['data'])

            if fileId is None:
                raise Exception("The request header does not contain fileId")

            response = saveMetadata.saveMetadata(fileId, data_attrib)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        #raise Http404
        context = {}
        return render(request, 'cyverseData.html', context)


def renderSharedData(request):
    response = None
    if request.is_ajax():
        try:
            #sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            response = getSharedData.getSharedData(path, limit, offset, sortcol, sortdir)
            #response = getSharedData.getSharedData('/iplant/home', '5', '0', 'NAME', 'ASC')
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

def getCommunityDataList(request):
    response = None
    if request.is_ajax():
        try:
            #sessionid = request.GET.get('sessionid')
            path = request.GET.get('url')
            limit = request.GET.get('limit')
            offset = request.GET.get('offset')
            sortcol = request.GET.get('sortcol')
            sortdir = request.GET.get('sortdir')
            response = getCommunityData.getCommunityData(path, limit, offset, sortcol, sortdir)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)


def getRootPathUser(request):
    response = None
    if request.is_ajax():
        try:
            #sessionid = request.GET.get('sessionid')
            accesstoken = settings.accesstoken
            response = getUserDetail.getUserDetails(accesstoken)
        except:
            e = sys.exc_info()
            return HttpResponse(e)
        return HttpResponse(response, content_type='application/json')
    else:
        # raise Http404
        context = {}
        return render(request, 'middlepanel.html', context)

