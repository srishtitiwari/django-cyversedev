var rpStatus = 0;
tableData=[];
$(document).ready(function (e) {
    rpStatus = 0;
    getFileList('', 20, 0, "NAME", "ASC")
    $("#actionButton").children().attr('disabled', 'true');
});
function getFileList(url,limit,offset,sortcol,sortdir){
//	var sessionid = Cookies.get('session');
//	if (sessionid==null) {loading(true);
	    $.ajax({
            method: "GET",
            dataType: "html",
        	async: "true",
			url: location.origin+"/base?url="+url+"&limit="+limit+"&offset="+offset+"&sortcol="+sortcol+"&sortdir="+sortdir,
	        success: function (data, status, jqXHR) {
	            intepretData(data, 'getFileList')
	            buildBreadCrumbs(data, 'getFileList')
            },
            error: function (jqXHR, status, err) {
                alert(err)
			},
            complete: function (jqXHR, status) {
            }
	    });
//    }
}

function getShareFileList(url,limit,offset,sortcol,sortdir){
//	var sessionid = Cookies.get('session');
//	if (sessionid==null) {loading(true);
	    $.ajax({
            method: "GET",
            dataType: "html",
        	async: "true",
			url: location.origin+"/renderSharedData?url="+url+"&limit="+limit+"&offset="+offset+"&sortcol="+sortcol+"&sortdir="+sortdir,
	        success: function (data, status, jqXHR) {
	            intepretData(data, 'getShareFileList')
	            buildBreadCrumbs(data, 'getShareFileList')
            },
            error: function (jqXHR, status, err) {
                alert(err)
			},
            complete: function (jqXHR, status) {
            }
	    });
//    }
}

function getCommunityData(url,limit,offset,sortcol,sortdir){
//	var sessionid = Cookies.get('session');
//	if (sessionid==null) {loading(true);
	    $.ajax({
            method: "GET",
            dataType: "html",
        	async: "true",
			url: location.origin+"/getCommunityData?url="+url+"&limit="+limit+"&offset="+offset+"&sortcol="+sortcol+"&sortdir="+sortdir,
	        success: function (data, status, jqXHR) {
	            intepretData(data, 'getCommunityData')
	            buildBreadCrumbs(data, 'getCommunityData')
            },
            error: function (jqXHR, status, err) {
                alert(err)
			},
            complete: function (jqXHR, status) {
            }
	    });
//    }
}

function intepretData(data, functiontype){
    var homePath="/iplant/home/pawanbole"
    var homePathforShare = '/iplant/home'
    var homePathforCommunity = '/iplant/home/shared'

    parsedData=$.parseJSON(data);
    $('#fileFolder').children('tbody').html("")
    populateTable(parsedData["folders"],"folders")
    populateTable(parsedData["files"],"files")
    $("tr").click(function () {
        $("tr").removeClass('active');
        $(this).addClass('active');
        $("#actionButton").children().removeAttr("disabled");
    });
    $("tr").dblclick(function () {
        if($(this).hasClass("folder").toString()=="true"){
            if(functiontype == 'getFileList') {
                var fldrpth=$(this).data("info").path.split(homePath)[1];
                getFileList(fldrpth, 10, 0, "NAME", "ASC")
            }else if (functiontype == 'getShareFileList'){
                var fldrpth=$(this).data("info").path
                getShareFileList(fldrpth, 10, 0, "NAME", "ASC")
            }else if (functiontype == 'getCommunityData'){
                var fldrpth=$(this).data("info").path
                getCommunityData(fldrpth, 10, 0, "NAME", "ASC")
            }
        }
    });
}

function populateTable(tableData,type) {
    var menuButton = '<button class="btn btn-success mr-1">View in IGB</button>\n' +
        '<button type="button" class="btn" data-toggle="dropdown"><img src="/static/img/3dot.png" height="25"  />\n' +
        '<div class="dropdown-menu" aria-labelledby="dropleftMenuButton">\n' +
        '<a class="dropdown-item" href="#" onclick="complete()"><i class="fas fa-user-plus mr-1"></i>Share</a>\n' +
        '<div class="dropdown-divider"></div>\n' +
        '<a class="dropdown-item" onclick="rightPanel(\'#makePublic\')" href="#"><i class="fas fa-link mr-1"></i>Manage Link</a>\n' +
        '<a class="dropdown-item" onclick="rightPanel(\'#metadata\')"href="#"><i class="fas fa-comment-alt mr-1"></i>Metadata</a>\n' +
        '<a class="dropdown-item" href="#" onclick="complete()"><i class="fas fa-search mr-1"></i>Analyse</a>\n' +
        '<div class="dropdown-divider"></div>\n' +
        '<a class="dropdown-item" href="#" onclick="complete()"><i class="fa fa-download mr-1"></i>Download</a>\n' +
        '</div>'
    if(type=="folders"){
        for (var data in tableData) {
            var row="<tr id='"+tableData[data].id+"' class='folder'><td><i class='fas fa-folder mr-1'></i>" + tableData[data].label + "</td><td>" + tableData[data].dateModified + "</td><td>" + tableData[data].fileSize + "</td><td></td></tr>"
            $('#fileFolder').children('tbody').append(row)
            $("#"+tableData[data].id).data("info",tableData[data])
        }
    }
    else if(type="files"){
         for (var data in tableData) {
            var row="<tr  id='"+tableData[data].id+"' class='file'><td title=" + tableData[data].label + "><i class='far fa-file mr-1'></i>" + tableData[data].label + "</td><td>" + tableData[data].dateModified + "</td><td>" + tableData[data].fileSize + "</td><td>" + menuButton + "</td></tr>"
            $('#fileFolder').children('tbody').append(row)
            $("#"+tableData[data].id).data("info",tableData[data])
         }
    }
}
function buildBreadCrumbs(data, functiontype){
    $(".breadcrumb").html('')
    var link=""
    parsedData=$.parseJSON(data);
    if(functiontype == 'getFileList') {
        var directories = parsedData.fileSystemInfo.path.split("/iplant/home/pawanbole/")[1]
    }else if(functiontype == 'getShareFileList'){
        var directories = parsedData.fileSystemInfo.path.split("/iplant/home/")[1]
    }else if(functiontype == 'getCommunityData'){
        var directories = parsedData.fileSystemInfo.path.split("/iplant/home/shared/")[1]
    }
3
    if(functiontype == 'getFileList') {
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getFileList(\'\',10,0,\'NAME\',\'ASC\')"><i class="fas fa-home mr-1"></i></a></li>')
        for (var dir in directories.split('/')) {
            link = link + "/" + directories.split('/')[dir]
            $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getFileList(\'' + link + '\',10,0,\'NAME\',\'ASC\')">' + directories.split('/')[dir] + '</a></li>')
        }
    }else if(functiontype == 'getShareFileList'){
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getShareFileList(\'\',10,0,\'NAME\',\'ASC\')"><i class="fas fa-home mr-1"></i></a></li>')
        for (var dir in directories.split('/')) {
            link = link + "/" + directories.split('/')[dir]
            $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getShareFileList(\'' + link + '\',10,0,\'NAME\',\'ASC\')">' + directories.split('/')[dir] + '</a></li>')
        }
    }else if(functiontype == 'getCommunityData'){
        $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getCommunityData(\'\',10,0,\'NAME\',\'ASC\')"><i class="fas fa-home mr-1"></i></a></li>')
        for (var dir in directories.split('/')) {
            link = link + "/" + directories.split('/')[dir]
            $(".breadcrumb").append('<li class=\"breadcrumb-item\"><a href=\'#\' onclick="getCommunityData(\'' + link + '\',10,0,\'NAME\',\'ASC\')">' + directories.split('/')[dir] + '</a></li>')
        }
    }
}
$(document).mouseup(function (e) {
    var table = $("table");
    var tr = $("tr");
    var td = $("td");
    var abtndiv = $("#actionButton");
    var abtn = $("#actionButton").children();
    var abtni = $("#actionButton").children().children();
    if (!table.is(e.target) && !td.is(e.target) && !td.is(e.target) && !abtndiv.is(e.target) && !abtn.is(e.target) && !abtni.is(e.target)) {
        $("tr").removeClass('active');
        abtn.attr('disabled', 'true');
    }
});

function rightPanel(data) {
    $(".middle").removeClass("col-10")
    $(".middle").addClass("col-7")
    $(".right").css("display", "block")
    rpStatus = 1
    $(".right").children().css("display", "none");
    $(data).css("display", "block");
}

function closerp() {
    $(".middle").removeClass("col-8")
    $(".middle").addClass("col-10")
    $(".right").css("display", "none")
    rpStatus = 0;
}

function complete() {
    alert("Congratulations! The Task is Complete.")
}

function viilinkOperations(ele) {
    if (ele == "remove") {
        $('#remove').css("display", "none")
        $('#create').css("display", "block")
        $('#plink').val("")
        $('#copy').css("visibility", 'hidden')
    } else {
        $('#create').css("display", "none")
        $('#remove').css("display", "block")
        $('#plink').val("http://agave.iplantc/nowlanf/test")
        $('#copy').css("visibility", 'visible')
    }
}



